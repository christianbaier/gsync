#!/usr/bin/env pipenv-shebang

#follow the steps on this page to receieve credentils.json from yout google grive:
#https://developers.google.com/drive/api/v3/quickstart/python#step_1_turn_on_the

from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
import glob
from datetime import datetime
import sys
import shutil
import json
import os


def authentificate(): 
    client_secrets_json = os.getcwd()+"/google_account/credentials.json"
    
    with open(client_secrets_json) as json_file:
        secrets = json.load(json_file)
    
    with open('mycreds.txt', 'w') as file:
        file.write(json.dumps(secrets)) 
    
    gauth = GoogleAuth()
    gauth.LoadCredentialsFile("mycreds.txt")
    if gauth.access_token_expired:
        gauth.Refresh()
    else: 
        gauth.Authorize()
    drive = GoogleDrive(gauth)
    
    os.remove('mycreds.txt')
    
    return drive


def gdrive_showall(folder=None):
    # iterate through all files and show tile, id and mimeType
    drive = authentificate()   # Bei gdrive anmelden
    for file_list in drive.ListFile({'q': 'trashed=false'}):#, 'maxResults': 10
        print(f'Received {len(file_list)} files from Files.list()') # <= 10
        for file1 in file_list:
            if folder in file1['title']:
                print(file1['title'])
                #print(f'title: {file1["title"]},  mimetype: {file1["mimeType"]} ,id: {file1["id"]}') #id: {file1["id"]},
            #else:
            #    print(f'title: {file1["title"]},  mimetype: {file1["mimeType"]} ,id: {file1["id"]}') #id: {file1["id"]},


def gtime2dt(gtime):
    gtime= gtime[:-1].replace('T',' ')
    return datetime.strptime(gtime, '%Y-%m-%d %H:%M:%S.%f')


def find_all_stored_folders():
    drive = authentificate()   # Bei gdrive anmelden
    #find all folder which are stored on gdrive
    all_gfolders = [[gfolder["title"],gfolder["id"]] for gfolder in drive.ListFile({"q": "mimeType='application/vnd.google-apps.folder' and trashed=false"}).GetList()]
    print(all_gfolders)


def upload_backup(folder2backup, *args):
    #folder2backup="/home/m0ps/backup"
    pcfiles = []
    originalfiles = []
    already_used=[]

    # collect all files which shall be uploaded
    for (dirpath, dirnames, filenames) in os.walk(folder2backup):
        pcfiles += [os.path.join(dirpath, file) for file in filenames]
        
    config_file = [x for x in pcfiles if '.gsync' in x]
    if config_file:
        gsync_folder = config_file[0].split('/')[-1].split('.')[0].replace('_','/')
        originalfiles = [x.replace(folder2backup,gsync_folder) for x in pcfiles]
    else:
        originalfiles = pcfiles
        gsync_folder = folder2backup

    orig_new = dict(zip(originalfiles,pcfiles))
    #new_orig = dict(zip(pcfiles,originalfiles))

    print('Authentificate')
    drive = authentificate()   # login to google drive
    
    for gfiles in drive.ListFile({'q': 'trashed=false'}):
        for gfile in gfiles:
            #print(gfile) 
            # if file already on server, check modification date
            if gfile['title'] in orig_new.keys():
                gd_mod_date = gtime2dt(gfile['modifiedDate'])
                pc_mod_date = datetime.fromtimestamp(os.path.getmtime(orig_new[gfile['title']]))
                
                # only upload existing files, only when they are newer than on gdrive
                if gd_mod_date > pc_mod_date:
                    f = drive.CreateFile({'id': gfile['id']})
                    f.Delete() 
                    f = drive.CreateFile({'title':gfile['title']})
                    f.SetContentFile(orig_new[gfile['title']])
                    print(f'{gfile["title"]} --> upload\n')
                    f.Upload()
                    already_used.append(orig_new[gfile['title']])
                else:
                    
                    # if file on pc is older than on google drive no upload
                    print(f'{gfile["title"]} -- NO UPLOAD! \nFile is older version than on google drive.\n')
                    already_used.append(orig_new[gfile['title']])

    # list of files which are not yet uploaded
    other_files = list(set(pcfiles)-set(already_used))
    for file in other_files:
        f = drive.CreateFile({'title':file.replace(folder2backup,gsync_folder)})
        #f['title'] = file.replace(folder2backup,gsync_folder)
        f.SetContentFile(file)        
        print(f'{file.replace(folder2backup,gsync_folder)} --> upload')
        f.Upload()   
    print(f'\n{len(other_files)} file(s) successfully uploaded!')

def create_pcfolders(fullpath):  
    #if folder not exist already create new          
    a = fullpath.split('/')[1:-1]
    path='/'  
    for folder in a:
        path += folder+'/'
        if not os.path.isdir(path):
            try:
                os.makedirs(path)
            except: pass    
   


def download_backup(folder2download,dest=None):    
    count=0
    pcfiles = []
    #folder2download='~/backup' #'/home/user/backup'
    #dest = '/home/user/aaa'
    home = os.path.expanduser("~")
    
    
    # substitue '~' with user home
    if folder2download[0] == '~':
        folder2download = home + folder2download[1:]
    creator = folder2download.split("/")[2]    
    
    # check if user is creator of backup to download
    if home not in folder2download and not dest:
        dest = input(f'folder created by user "{creator}", please specify folder to download: ')
        if dest[0] == '~':
            dest = home + dest[1:]
        if not os.path.isdir(dest):
            os.mkdir(dest)
    
    if not dest:
        dest = folder2download

    

    
    # check if already files in target pc folder
    for (dirpath, dirnames, filenames) in os.walk(dest):
        pcfiles += [os.path.join(dirpath, file) for file in filenames]
    
    print('Authentificate')
    drive = authentificate()   # login to google drive
     
    for gfiles in drive.ListFile({'q': 'trashed=false'}):
        for gfile in gfiles:
            gfile['title'] = gfile['title'].replace(folder2download,dest)
            
            # files already exist on pc
            if (dest in gfile['title']) and (gfile['title'] in pcfiles):
                gd_mod_date = gtime2dt(gfile['modifiedDate'])
                pc_mod_date = datetime.fromtimestamp(os.path.getmtime(gfile['title']))
                
                # only download existing files, when they are newer than on gdrive
                if gd_mod_date > pc_mod_date:
                    f = drive.CreateFile({'id': gfile['id']}) 
                    f.GetContentFile(gfile['title']) 
                    count +=1
                    print(f'{gfile["title"]} --> download\n')
                else:
                    print(f'{gfile["title"]} -- NO DOWNLOAD! \n google drive file is same or older version than on pc.\n')
            
            # when gfile does not exist in dest
            elif dest in gfile['title']:
                create_pcfolders(gfile['title'])
                f = drive.CreateFile({'id': gfile['id']}) 
                #gfile['title'] alter destination here
                f.GetContentFile(gfile['title']) 
                count +=1
                print(f'{gfile["title"]} --> download')

    if dest != folder2download: 
        with open(dest+f'/{folder2download.replace("/","_")}.gsync', 'w') as f: 
            f.write('Do not rename this config file! This file contains \n \
                    information of the original user who created this folder.') 
        print('data_origin file created!')
    if count == 0:
        print(f'\nno files downloaded to "{dest}". Folder is uptodate!')    
    else:
        print(f'\n{count} file(s) successfully downloaded to "{dest}"!')

def delete_backup(folder2delete, *args):
    #folder2delete='/home/user/backup/'
    f=[]
    print(f'Delete backup of {folder2delete} permanently?[y/n]')
    a = input('y for permanent delete!')
    if a == 'y':
        # Bei gdrive anmelden
        print('Authentificate')
        drive = authentificate()   
        #folder2delete= folder2delete.split('/')[-2] 
        gfiles=drive.ListFile({'q':f"trashed=false"}).GetList()
        for gfile in gfiles:
            #print(folder2delete,gfile['title'])
            if folder2delete in gfile['title']:
                #print(folder2delete,gfile['title'])
                f = drive.CreateFile({'id': gfile['id']}) 
                print(f'{gfile["title"]} --> DELETE')
                f.Delete()  # Permanently delete the file.  
        if not f:
            print(f'\n"{folder2delete}" not found in google drive!')
    else:
        print('\naction canceled by user!')
        sys.exit(0)

def delete_local(folder2delete, *args):
    from shutil import rmtree
    #folder2delete='/home/user/aaa/'
    
    home = os.path.expanduser("~")    
    # substitue '~' with user home
    if folder2delete[0] == '~':
        folder2delete = home + folder2delete[1:]
        
    if os.path.isdir(folder2delete):
        print(f'Delete directory "{folder2delete}" and all subfolders permanently from pc?[y/n]')
        a = input('y for permanent delete, other other keys for escape!')
        if a == 'y':
            rmtree(folder2delete) # Permanently delete the folder and all subfolders  
            print(f'folder "{folder2delete}" and subfolders deleted!')
        else:
            print('\naction canceled by user!')
            sys.exit(0)
            
    else:
        print(f'\nno such directory found: "{folder2delete}"')
        sys.exit(1)


def whats_the_job(cmds):
    cmds = cmds[1:] # remove scriptpath
    dispatcher = {'upload_backup': upload_backup, 
                  'download_backup': download_backup,
                  'delete_backup': delete_backup,
                  'delete_local': delete_local}
    
    if 'help' in cmds:
        print('\nCommands for using this script:\n')
        print('Please specify PATH  i.e.  /home/user/folder')
        print('upload_backup PATH  ' + 'uploads local folder to googledrive')
        print('download_backup PATH  ' + 'downloads backup from googledrive to local harddisk')
        print('delete_backup PATH  '+ 'deletes backup on googledrive, local folder is not deleted!')
        print('delete_local PATH  ' + 'deletes local folder, backup on gdrive is not deleted!')

        sys.exit(1)
        
    path = [x for x in cmds if '/' in x]
    if len(path) == 1:
        path.append('')
    
    #print('path:')
    #print(path)
    #print(cmds)
    
    if not path:
        print('\nPlease specify path. Type "help" for help!\n')
        sys.exit(1)
    
    cmds.remove(path[0])
       
    if len(path) < 2:
        path.append(None)

    if not cmds:
        print('\nSorry, no commands found!\n')
        sys.exit(1)
        
    if len(cmds) >2:
        print('\nSorry, too many arguments. Use "gsync help" for help!\n')
        sys.exit(2)
    
    if cmds[0] in dispatcher.keys():
        #Funktionsaufruf
        dispatcher[cmds[0]](path[0],path[1])
    else:
        print('\nSorry, command not recognized. Use "gsync help" for help!\n')
        
    
if __name__ == "__main__":
    startTime = datetime.now()
    
    whats_the_job(sys.argv)
    
    print()
    print(f'Duration: {datetime.now() - startTime}')
    