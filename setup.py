#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 11 10:51:40 2020

@author: user
"""

#setup.py
import shutil
import os
from datetime import datetime
import glob
from os.path import expanduser
import sys
import subprocess as sp


if __name__ == "__main__":
    startTime = datetime.now()
    
    required_files = ['Pipfile.lock','Pipfile','gsync.py','README.txt']
    home = expanduser("~")
    wd = os.getcwd()
    
    src = input(f'downloaded "gsync" files in folder "{wd}" ?[y/n]: ')
    src = src.lower()
    if src =='n':
        src = input('please specify path of downloaded "gsync" files: ')
    else:
        src = wd
        
    files = glob.glob(src+'/**')
    
    files =[x for x in files if 'setup.py' not in x and os.path.isfile(x)]
    
    # check if all needed files will be copied
    if len([True for x in required_files for y in files if x in y]) != 5:
        print()
        print('files are missing. Correct path?')
        print()
        sys.exit(1)
    
    print()
    print('found all required files to copy:')
    print()
    [print(x) for x in files]
    print()
    print(f'recommended installation folder for "gsync" is: {home}/bin/gsync')
   
    r_dest = home+'/bin/gsync'
    dest = input(f'install "gsync" in path "{r_dest}" ?:[y/n] ')
    
    if not dest or dest.lower() == 'y':
        dest = r_dest
    else:
        dest = input('please specify path to install "gsync": ')
    
    if not os.path.exists(dest):
        os.makedirs(dest, exist_ok=True)
        os.makedirs(dest+'/google_account', exist_ok=True)
    else:
        print()
        inp = input('path already exists. Overwrite?[y/n]')
        inp = inp.lower()
        if inp == 'n':
            print('process aborted by user.')
            sys.exit(1)
        print()
        
        
    for file in files:
        shutil.copy(file,dest)    
    print(f'all {len(files)} files copied to {dest}. Folder "google_account/" created')
    
    # install pipenv and pipenv-shebang to system
    print()
    print('install pipenv to system')
    stream =  os.popen('pip3 install pipenv && pip3 install pipenv-shebang')
    output = stream.read()
    print(output)
    
    # install pipenv to destination directory
    print('create pipenv environment')
    stream = os.popen(f'cd {dest} && pipenv install')
    output = stream.read()
    print(output)
    print('Pipenv environment created!')

    
    inp = input('create global alias "gsync"? [y/n]')
    if inp.lower() == 'y':
        if os.path.isfile(home+'/.bash_aliases'):
            with open(home+'/.bash_aliases', 'r') as f:
                if f'gsync.py' in f.read():
                    print('gsync already in ~/.bash_aliases file')
                    sys.exit(1)
        with open(home+'/.bash_aliases', 'a') as f:
            f.write(f'alias gsync="pipenv-shebang {dest}/gsync.py"')
        
        stream = os.popen(f'. ~/.bash_aliases')
        
        print('Alias created. Type "gsync help" in terminal to start!')

    else:
        print('Ready to go!')
    print('Please close this terminal session.')
