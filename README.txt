README

0) prequisits:
   - pip3 installed
   - ~/.bash_aliases either not existent or writable

1) run "python3 setup.py" and follow instructions. 
   Alias creation recommended at the end of the installation.
   

2) Get google api credentials:
   - log in to your google account
   - go to https://developers.google.com/drive/api/v3/quickstart/python
   - click on button "Enable the Drive API"
   - select "Desktop app" 
   - press "DOWNLOAD CLIENT CONFIGURATION"
   - save "credentials.json" in the folder "/gsync/google_account"


Usage:
- Start Terminal
- type in "gsync help" for further instructions 